<?php

include ("settings.php");
include ("../inc/Parsedown.php");
include ("../inc/read_contents.php");

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */

$current_step = $_GET['step'];

foreach ($step as $step_row => $step_item) {
	
	if ($step_item[0]["key"] == $current_step) {
		
		$current_module = $step_item[0]["module"];
		$current_section = $step_item[0]["section"];
		$current_name = $step_item[0]["name"];
		
	}
	
}

?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<?php include ("../inc/1177-header-meta.php"); ?>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			<div style="background: #473b3c; display:block; margin:0;">
				<div style="display:block;padding:10px 30px;margin:0 auto;max-width:960px;">
				
				<h1 style="color: #fff;font-size:1.1em;margin:0;padding:0;">DEMO: Stöd och behandling</h1>
				
				</div>
				
			</div>
			<div style="background: #655c5b; display:block; margin:0;">
				<div style="display:block;padding:10px 30px;margin:0 auto;max-width:960px;color:#fff;">
				
				Inloggad som behandlare
				
				</div>
				
			</div>
			
			<?php include ("../inc/1177-breadcrumbs.php"); ?>
			
			
			<!--<div class="heading-container">
				<div class="heading">
					<h1><?php echo $moment_name; ?></h1>
				</div>
			</div>-->
			
			
			<?php $active_home = true; include ("../inc/1177-navtabs.php"); ?>
			
			
			<div class="row-main">
				<div class="main-wide square">
										
					
					
					<div class="patient-header" style="margin:20px 30px;">
						<div style="border-bottom:1px solid #999;margin:0;">
							<h2 style="display:inline-block;margin:0;padding:0;"><?php echo $patient_name ?></h2> 19580830-8364
						</div>
						
						<div style="margin:8px 0;">
							<h3 style="margin:0;padding:0;font-weight:bold;line-height:1em;">Social fobi</h3>
							<span style="color:#999;">version 1.0</span>
						</div>
					</div>
					
					
					<?php  include ("../inc/1177-navtabs2.php"); ?>
					
					
					<div class="content-level2" style="background: #faf8f7;display:block;margin: 0 -15px -15px; padding:20px;border-top:1px solid #e5dedb;z-index:-1;">
					
						<a href="patient-manage.php"><i class="fas fa-arrow-left"></i> Tillbaka</a>
						
						<div class="spacer16"></div>
						
						<h2 style="margin-top:0;"><?php echo $current_name; ?></h2>
						
						<?php
						
							$filename = '..' . $moment[0] . 'contents/' . $current_step . '.txt';

							if (file_exists($filename)) {
								$current_content = file_get_contents($filename);
								
							} else {
								//file_put_contents($filename, $step_empty);
								$current_content = $step_empty;
							}
							
							echo $current_content;
						
						?>
						
											
						
						
						
						<div class="pagination-container">
							<div class="pagination-inside">
							
								<ul class="pagination-list">
							
								<?php
								
								$page_counter = 1;
								foreach ($step as $step_row => $step_item) {
									
									$current_path = explode('.',$current_step);
									$needle_path = explode('.',$step_item[0]["key"]);
									
									$is_active = '';
									if ($current_step == $step_item[0]["key"]) {
										
										$is_active = "is-active";
									}
									
									
									if ($needle_path[0] == $current_path[0]) { ?>
										
										
										<li class="pagination-item <?php echo $is_active; ?>" ><a href="patient-step.php?step=<?php echo $step_item[0]["key"]; ?>" title="<?php echo $step_item[0]["name"]; ?>"><?php echo $page_counter; ?></a></li>
										
										
									<?php 
									
										$page_counter++; 
									}
									
									
									
								} ?>
							
								</ul>
								
								<div style="margin-top:10px;"><a href="patient-manage.php">Tillbaka till översikten</a></div>
							</div>
						</div>

					</div>
					
					
					
					<hr style="margin:32px -15px 16px">
					
					<h3 style="font-weight:bold;">Sidkommentar</h3>

					
					<div class="expandable-container is-messages">
					
						<div class="expandable-header is-messages">
						
							<div class="exh-icon">
								<i class="far fa-comment" data-fa-transform="grow-4"></i>
							</div>
						
							<div class="exh-guide">
								<h3><?php echo $current_section . ' / ' . $current_name; ?></h3>
							</div>
							
							<div class="exh-extra" style="margin-top:5px;">
								
								<div class="exh-info">
									
								</div>
							
								<div class="exh-controls">
									<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
									<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
								</div>
							
							</div>
							
							<div class="exh-row2">
								<small>Senaste meddelandet 2018-02-28 15:02</small>
							</div>
						
						</div><!--expandable-header-->
						
						<div class="expandable-content is-messages">
						
						
							<div class="sendmessage-container">
							
								<div class="sendmessage-inside">
									
									<form class="form-tight">
									<h5 style="margin:0;">Skriv meddelande</h5>
									
									<textarea style="width:99%;height:20px;" class="expand"></textarea>
									</form>
								
									<div>
									
										<div style="margin:8px 10px;display:inline-block;color:#2e6aa3;"><i class="fas fa-external-link-alt" data-fa-transform="grow-12"></i></div>
										<div style="display:inline-block;float:right;"><a class="button" style="padding:6px 12px;margin:2px;">Skicka</a></div>
										
									</div>
								
								</div>

								
							</div><!--sendmessage-container-->
							
							<hr>
						
							<div class="messagebox is-doctor">
							
								<div class="messagebox-header">
								
									<strong><?php echo $patient_name; ?></strong> <small>2018-02-28 15:02</small>
								
								</div>
							
								<div class="messagebox-body">
								
									Tack! Nu börjar jag få koll på det här verktyget. 
								</div>
							
							</div><!--messagebox-->
							
							<div class="messagebox is-patient">
							
								<div class="messagebox-header">
								
									<strong><?php echo $doctor_name; ?></strong> <small>2018-02-14 09:37</small>
								
								</div>
							
								<div class="messagebox-body">
								
									Fusce eget arcu ac nunc pharetra sagittis. Duis feugiat nisi vel sem fermentum, eu sodales mi interdum. Etiam nec tortor semper neque eleifend luctus. Fusce urna sapien, blandit ut blandit in, tincidunt id magna. Ut iaculis nibh eget maximus dignissim. Proin dignissim ipsum velit, et malesuada velit egestas ut. 
								</div>
							
							</div><!--messagebox-->
							
							<div style="clear:both;"></div>
						
						</div><!--expandable-content-->
					
					</div><!--expandable-container-->
										
										
					
				</div><!--main-wide-->
			</div><!--row-main-->
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<?php include '../inc/sob-footer-block.php'; ?> 
		 
		
	</body>
</html>