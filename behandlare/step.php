<?php

include ("settings.php");
include ("../inc/Parsedown.php");
include ("../inc/read_contents.php");

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */

$current_step = $_GET['step'];

$visited = $_SESSION["visited_steps"];

if (strpos($visited, $current_step) === false) {
	
	$_SESSION["visited_steps"] .= "$current_step | " ;
}





foreach ($step as $step_row => $step_item) {
	
	if ($step_item[0]["key"] == $current_step) {
		
		$current_module = $step_item[0]["module"];
		$current_section = $step_item[0]["section"];
		$current_name = $step_item[0]["name"];
		
	}
	
}


?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<?php include ("../inc/1177-header-meta.php"); ?>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			<?php include ("../inc/1177-header-block.php"); ?>
			
			<?php include ("../inc/1177-breadcrumbs.php"); ?>
			
			
			<div class="heading-container">
				<div class="heading">
					<h1><?php echo $moment_name; ?></h1>
				</div>
			</div>
			
			
			<?php 
			$active_home = true;
			include ("../inc/1177-navtabs.php"); ?>
			
			
			<div class="row-main">
				<div class="main-wide square step-contents">
										

					<h2 style="margin-top:0;"><?php echo $current_name; ?></h2>
					
					<?php
					
						$filename = 'contents/' . $current_step . '.txt';

						if (file_exists($filename)) {
							$current_content = file_get_contents($filename);
							
						} else {
							file_put_contents($filename, $step_empty);
							$current_content = $step_empty;
						}
						
						echo $current_content;
					
					?>
					
										
					
					
					
					<div class="pagination-container">
						<div class="pagination-inside">
						
							<ul class="pagination-list">
						
							<?php
							
							$page_counter = 1;
							foreach ($step as $step_row => $step_item) {
								
								$current_path = explode('.',$current_step);
								$needle_path = explode('.',$step_item[0]["key"]);
								
								$is_active = '';
								if ($current_step == $step_item[0]["key"]) {
									
									$is_active = "is-active";
								}
								
								
								if ($needle_path[0] == $current_path[0]) { ?>
									
									
									<li class="pagination-item <?php echo $is_active; ?>" ><a href="step.php?step=<?php echo $step_item[0]["key"]; ?>" title="<?php echo $step_item[0]["name"]; ?>"><?php echo $page_counter; ?></a></li>
									
									
								<?php 
								
									$page_counter++; 
								}
								
								
								
							} ?>
						
							</ul>
							
							<div style="margin-top:10px;"><a href="<?php echo $home_link; ?>">Tillbaka till översikten</a></div>
						</div>
					</div>
					
					
					
					
				</div><!--main-wide-->
			</div><!--row-main-->
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<?php include '../inc/1177-footer-block.php'; ?>
		 
		<script src="https://sob.udo.se/js/demo-footer.js?003"></script>
		
	</body>
</html>