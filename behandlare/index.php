<?php

include ("settings.php");
include ("../inc/Parsedown.php");
include ("../inc/read_contents.php");

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */


?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<?php include ("../inc/1177-header-meta.php"); ?>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			<div style="background: #473b3c; display:block; margin:0;">
				<div style="display:block;padding:10px 30px;margin:0 auto;max-width:960px;">
				
				<h1 style="color: #fff;font-size:1.1em;margin:0;padding:0;">DEMO: Stöd och behandling</h1>
				
				</div>
				
			</div>
			<div style="background: #655c5b; display:block; margin:0;">
				<div style="display:block;padding:10px 30px;margin:0 auto;max-width:960px;color:#fff;">
				
				Inloggad som behandlare
				
				</div>
				
			</div>
			
			<?php include ("../inc/1177-breadcrumbs.php"); ?>
			
			
			<!--<div class="heading-container">
				<div class="heading">
					<h1><?php echo $moment_name; ?></h1>
				</div>
			</div>-->
			
			
			<?php include ("../inc/1177-navtabs.php"); ?>
			
			
			<div class="row-main">
				<div class="main-wide square">
										
					
					
					<div class="topcontrols-container" style="margin-bottom:8px;">
						<div class="topcontrols-inside" style="text-align:right;">
							<a href="/popup/start-moment.php" class="button is-tight ajax-popup-link">Starta nytt moment</a>
						</div>
					</div>
					

					
					<div class="expandable-container is-filter">
						<div class="expandable-header is-filter">
						
							
							<div class="exh-guide">
								<h4>Filter</h4>
							</div><!--exh-guide-->
						
						
							<div class="exh-extra" style="margin-top:3px;">
								
								<div class="exh-info">
									
								</div>
							
								<div class="exh-controls">
									<i class="fas fa-angle-down control-open" data-fa-transform="grow-20"></i>
									<i class="fas fa-angle-up control-close" data-fa-transform="grow-20"></i>
								</div>
							
							</div><!--exh-extra-->
						
						</div><!--expandable-header-->
						
						<div class="expandable-content is-filter">
							<form>
							
								<div class="col1">
									<label>Moment</label>
									<select class="is-wide">
										<option value="alla">Visa alla</option>
										<option value="/social-fobi/">Social fobi</option>
									</select>
									
									<label>Flaggor</label>
									<select class="is-wide">
										<option value="alla">Visa både flaggade och oflaggade</option>
										<option value="flaggade">Visa endast flaggade</option>
										<option value="oflaggade">Visa endast oflaggade</option>
										<option value="" disabled>Aktivitetsplan</option>
										<option value="" >&nbsp;&nbsp;Aktiviteter som sparats</option>
										<option value="" >&nbsp;&nbsp;Missad aktivitet</option>
										<option value="" >&nbsp;&nbsp;Registrering över/under målvärde</option>
										<option value="" disabled>Moment</option>
										<option value="" >&nbsp;&nbsp;Nytt meddelande/kommentar</option>
										<option value="" >&nbsp;&nbsp;Invånare inaktiv</option>
										<option value="" >&nbsp;&nbsp;Startat moment</option>
										<option value="" >&nbsp;&nbsp;Moment snart slut</option>
										<option value="" >&nbsp;&nbsp;Byte av behandlare</option>
										<option value="" >&nbsp;&nbsp;Påminnelse på datum</option>
										<option value="" disabled>Formulär</option>
										<option value="" >&nbsp;&nbsp;Formulär som sparats</option>
										<option value="" >&nbsp;&nbsp;Formulär som måste hanteras</option>
										<option value="" >&nbsp;&nbsp;Formulär i mätbatteri som sparats</option>
										<option value="" >&nbsp;&nbsp;Uppmärksamma resultat</option>
										<option value="" >&nbsp;&nbsp;Uppmärksamma resultat (prioriterade)</option>

									</select>
									
									<label>Fritext</label>
									<input type="text" class="is-wide"></input>
									
								</div><!--col1-->
								<div class="col2">
								
									<div style="display:block;text-align:right;">
										<a class="button is-tight tooltip" data-tooltip-content="#save_filter">Spara filter <i class="fas fa-caret-down"></i></a>
									</div>
								
								</div><!--col2-->
						
						</div><!--expandable-content-->
					</div><!-- expandable-container -->
					
					
					
					
					
					<div class="expandable-container is-momentlist">
						<div class="expandable-header activateonload">
						
							
							<div class="exh-guide">
								<h4 style="font-size:1.4em;">Social fobi</h4>
							</div><!--exh-guide-->
						
						
							<div class="exh-extra" style="margin-top:-6px;">
								
								<div class="exh-info">
									<button class="button is-tight" style="border:0;">Starta moment</button>
								</div>
							
								<div class="exh-controls">
									<i class="fas fa-angle-down control-open" data-fa-transform="grow-20"></i>
									<i class="fas fa-angle-up control-close" data-fa-transform="grow-20"></i>
								</div>
							
							</div><!--exh-extra-->
						
						</div><!--expandable-header-->
						
						<div class="expandable-content">
							
							<table class="people-table">
							
							<thead>
								<tr>
									<th>Namn</th>
									<th>Personnummer</th>
									<th>Momentstart</th>
									<th>Senast aktiv</th>
									<th>Flaggor</th>
								</tr>
							</thead>
							<tbody>
								<tr class="tr-odd" style="">
									<td><a href="patient-manage.php">Ulla Tufvesson</a></td>
									<td>19580830-8364</td>
									<td>
										
										<?php 
										
										
											$dateget = date('Y-m-d');
											$sdate = strtotime ( '-36 day' , strtotime ( $dateget ) ) ;
											$sdate = date ( 'Y-m-d' , $sdate );

											echo $sdate;
										
										
										?>
										
									</td>
									
									<td>
									<?php 
									
									
										$dateget = date('Y-m-d');
										$sdate = strtotime ( '-5 day' , strtotime ( $dateget ) ) ;
										$sdate = date ( 'Y-m-d' , $sdate );

										echo $sdate;
									
									
									?>
									</td>
									
									<td>
									
										<i class="fas fa-clock flag-ok"></i> <i class="fas fa-list-ul flag-ok"></i>
									
									</td>
									
								</tr>
								
								
								<tr class="tr-even" style="">
									<td><a href="patient-manage.php">Kaarina Åkerberg</a></td>
									<td>19910612-8581</td>
									<td>
										
										<?php 
										
										
											$dateget = date('Y-m-d');
											$sdate = strtotime ( '-28 day' , strtotime ( $dateget ) ) ;
											$sdate = date ( 'Y-m-d' , $sdate );

											echo $sdate;
										
										
										?>
										
									</td>
									
									<td>
									<?php 
									
									
										$dateget = date('Y-m-d');
										$sdate = strtotime ( '-7 day' , strtotime ( $dateget ) ) ;
										$sdate = date ( 'Y-m-d' , $sdate );

										echo $sdate;
									
									
									?>
									</td>
									
									<td>
									
										<i class="fas fa-clock flag-ok"></i> <i class="fas fa-list-ul flag-ok"></i>
									
									</td>
									
								</tr>

							</tbody>
						
						</div><!--expandable-content-->
					</div><!-- expandable-container -->


										
					
				</div><!--main-wide-->
			</div><!--row-main-->
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<?php include '../inc/sob-footer-block.php'; ?>
		
		
		<div class="tooltip-templates">
		
			<div id="save_filter">
				<form>
				<input type="text"></input>
				Avbryt
			</div>
		
		
		</div>
		
	</body>
</html>