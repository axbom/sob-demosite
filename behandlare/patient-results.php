<?php

include ("settings.php");
include ("../inc/Parsedown.php");
include ("../inc/read_contents.php");

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */


?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<?php include ("../inc/1177-header-meta.php"); ?>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			<div style="background: #473b3c; display:block; margin:0;">
				<div style="display:block;padding:10px 30px;margin:0 auto;max-width:960px;">
				
				<h1 style="color: #fff;font-size:1.1em;margin:0;padding:0;">DEMO: Stöd och behandling</h1>
				
				</div>
				
			</div>
			<div style="background: #655c5b; display:block; margin:0;">
				<div style="display:block;padding:10px 30px;margin:0 auto;max-width:960px;color:#fff;">
				
				Inloggad som behandlare
				
				</div>
				
			</div>
			
			<?php include ("../inc/1177-breadcrumbs.php"); ?>
			
			
			<!--<div class="heading-container">
				<div class="heading">
					<h1><?php echo $moment_name; ?></h1>
				</div>
			</div>-->
			
			
			<?php $active_home = true; include ("../inc/1177-navtabs.php"); ?>
			
			
			<div class="row-main">
				<div class="main-wide square">
										
					
					
					<div class="patient-header" style="margin:20px 30px;">
						<div style="border-bottom:1px solid #999;margin:0;">
							<h2 style="display:inline-block;margin:0;padding:0;"><?php echo $patient_name ?></h2> 19580830-8364
						</div>
						
						<div style="margin:8px 0;">
							<h3 style="margin:0;padding:0;font-weight:bold;line-height:1em;">Social fobi</h3>
							<span style="color:#999;">version 1.0</span>
						</div>
					</div>
					
					
					<?php  include ("../inc/1177-navtabs2.php"); ?>
					
					
					<div class="content-level2" style="background: #faf8f7;display:block;margin: 0 -15px -15px; padding:20px;border-top:1px solid #e5dedb;z-index:-1;">
					
						
						<h2 style="margin-top:0;">Resultat</h2>
							
						<div calss="spacer16"></div>
						
						<div class="expandable-container">
						
							<div class="expandable-header">
							
								
							
								<div class="exh-guide">
									<h3>Sömnloggen</h3>
								</div>
								
								<div class="exh-extra" style="margin-top:5px;">
									
									<div class="exh-info">
										
									</div>
								
									<div class="exh-controls">
										<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
										<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
									</div>
								
								</div>
								
								
							
							</div><!--expandable-header-->
							
							<div class="expandable-content is-primary">
							
							
								
								<h3>Antal timmar sömn</h3>
								
								<div class="col1">
								
									<img src="/img/diagram-sleep.png" alt="diagram" style="max-width:100%">
								
								</div>
								
								<div class="col2">
								
									<table class="data-table">
										<thead>
											<tr>
												<th>Datum/tid</th><th>Värde</th>
											</tr>
										</thead>
										<tbody>
											
												<tr><td>2018-04-18</td><td>5</td></tr>
												<tr><td>2018-04-19</td><td>4</td></tr>
												<tr><td>2018-04-20</td><td>7</td></tr>
												<tr><td>2018-04-21</td><td>3</td></tr>
												<tr><td>2018-04-22</td><td>8</td></tr>
												<tr><td>2018-04-23</td><td>6</td></tr>
											
										</tbody>
									</table>
								
								</div>
								
							
							</div><!--expandable-content-->
						
						</div><!--expandable-container-->

					
					
					</div>
					
					
										
										
					
				</div><!--main-wide-->
			</div><!--row-main-->
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<?php include '../inc/sob-footer-block.php'; ?> 
		 
		
	</body>
</html>