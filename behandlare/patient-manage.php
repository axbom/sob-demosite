<?php

include ("settings.php");
include ("../inc/Parsedown.php");
include ("../inc/read_contents.php");

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */


?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<?php include ("../inc/1177-header-meta.php"); ?>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			<div style="background: #473b3c; display:block; margin:0;">
				<div style="display:block;padding:10px 30px;margin:0 auto;max-width:960px;">
				
				<h1 style="color: #fff;font-size:1.1em;margin:0;padding:0;">DEMO: Stöd och behandling</h1>
				
				</div>
				
			</div>
			<div style="background: #655c5b; display:block; margin:0;">
				<div style="display:block;padding:10px 30px;margin:0 auto;max-width:960px;color:#fff;">
				
				Inloggad som behandlare
				
				</div>
				
			</div>
			
			<?php include ("../inc/1177-breadcrumbs.php"); ?>
			
			
			<!--<div class="heading-container">
				<div class="heading">
					<h1><?php echo $moment_name; ?></h1>
				</div>
			</div>-->
			
			
			<?php $active_home = true; include ("../inc/1177-navtabs.php"); ?>
			
			
			<div class="row-main">
				<div class="main-wide square">
										
					
					<div class="patient-header" style="margin:20px 30px;">
						<div style="border-bottom:1px solid #999;margin:0;">
							<h2 style="display:inline-block;margin:0;padding:0;"><?php echo $patient_name ?></h2> 19580830-8364
						</div>
						
						<div style="margin:8px 0;">
							<h3 style="margin:0;padding:0;font-weight:bold;line-height:1em;">Social fobi</h3>
							<span style="color:#999;">version 1.0</span>
						</div>
					</div>
					
					
					<?php  include ("../inc/1177-navtabs2.php"); ?>
					
					
					<div class="content-level2" style="background: #faf8f7;display:block;margin: 0 -15px -15px; padding:20px;border-top:1px solid #e5dedb;z-index:-1;">
					
					
					
					
						<div class="patientprogress-container" style="margin-bottom:16px; width:100%;position:relative;">
							<div class="patientprogress-inside">
							
								<div class="progressbar-container" style="display:inline-block; vertical-align:middle; border:1px solid #999;width:20%;background:#fff;">
									
									<div class="progressbar-inside" style="border:1px solid #fff;width:80%;background:#b6d38f">
									
										&nbsp;
									</div>
									
									
								</div>
								
								<div class="patientprogress-info" style="display:inline-block; vertical-align:middle;">
									&nbsp;&nbsp;<strong>Dagar i momentet:</strong> 27 (rek. 30 dagar) &nbsp;&nbsp;<strong>Moduler avklarade:</strong> 2 av 10
								</div>
								
								
								<div class="patientprogress-features" style="display:inline-block; vertical-align:middle;float:right;">
									<a class="footer-link-text link1177 ajax-popup-link" href="/popup/demo-warning.php" target="_blank" title="Öppnas i nytt fönster">Egenskaper</a>
								</div>
								
								
							
							</div>
						</div>
					
						<div class="patientmeta-container" style="border:1px solid #999;margin-bottom:16px;">
							<div class="patientmeta-inside" style="background:#fff;padding:8px 16px;">
							
								<div class="col1">
									<strong>Senast aktiv:</strong> 2018-03-15<br>
									<strong>Moment startade:</strong> 2018-01-02<br>
									
								</div>
								
								<div class="col1">
									<strong>Ansvarig behandlare:</strong> 2018-03-15<br>
									<strong>Telefonnummer:</strong> ej inlagt<br>
								</div>
							
							</div>
						</div>
						
						
						<div style="display:block;">
							<div style="float:right;">
								<a class="footer-link-text link1177 ajax-popup-link" href="/popup/demo-warning.php" target="_blank" title="Öppnas i nytt fönster">Se historik i tidslinjen</a>
							</div>
							<div style="clear:both;"></div>
						</div>
						
						
						<div class="patientflags-container" style="border:1px solid #999;">
							<div class="patientflags-inside" style="background:#e5ded8;">
							
								<div class="col-1-3" style="display:inline-block; vertical-align:top; width:30%;background:#e5ded8;padding:0 8px;">
								
									<h5>Aktiviteter</h5>
									<i class="fas fa-list-ul flag-ok"></i>
								
								</div>
								<div class="col-2-3" style="display:inline-block; vertical-align:middle; width:30%;background:#e5ded8;padding:0 8px;">
								
									<h5>Övriga händelser<h5>
									<i class="fas fa-clock flag-ok"></i>
								</div>
								<div class="col-3-3" style="display:inline-block; vertical-align:middle; width:30%;background:#e5ded8;padding:0 8px;">
								
									<h5>&nbsp;</h5>
								
								</div>
							
							</div>
						</div>
						
						
						<div class="spacer16"></div>
						
						<div class="expandable-container is-caregiverinfo">
						
							<div class="expandable-header">
							
								<div class="exh-guide">
									<h3>Info från vårdgivaren</h3>
								</div>
								
								<div class="exh-extra" style="margin-top:5px;">
									
									<div class="exh-info">
										
									</div>
								
									<div class="exh-controls">
										<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
										<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
									</div>
									
									
								
								</div>
							
							</div>
							
							<div class="expandable-content">
							
								<p><?php echo $info_caregiver; ?></p>
								<div style="display:block;text-align:right;">
									<button class="button is-tight ajax-popup-link" href="">Redigera text</button>
								</div>
							
							</div>
						</div>
						
						
						<div class="spacer16"></div>
						
						<div class="expandable-container is-managep">
						
							<div class="expandable-header">
							
								<div class="exh-guide">
									<h3>Aktuella moduler</h3>
								</div>
								
								<div class="exh-extra" style="margin-top:5px;">
									
									<div class="exh-info">
										
									</div>
								
									<div class="exh-controls">
										<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
										<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
									</div>
								
								</div>
							
							</div>
							
							<div class="expandable-content">
							
								
								
								<?php 
								
									$module_count=1;
									foreach ($module as $module_item) {
										
										
										
										
										?>
										
										
										
										
										<div class="expandable-container is-modulep">
										
											<div class="expandable-header">
											
											
												<div class="exh-guide">
													<h3><?php echo $module[$module_count] ?></h3>
												</div>
												
												<div class="exh-extra">
													
													<div class="exh-info">
														<button class="button is-small activate-completed">Sätt till avklarad</button>
														&nbsp;&nbsp;
														dagar kvar: 7 &nbsp;&nbsp;
														<i class="fas fa-pencil-alt" data-fa-transform="grow-1"></i>
													</div>
												
													<div class="exh-controls">
														<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
														<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
													</div>
												
												</div>
											
											</div><!--expandable-header-->
											
											<div class="expandable-content is-primary">
											
											
												<div class="modulepset">
													<div class="col1">
														<label style="display:inline-block;">Invånarstatus</label>
														<select style="display:inline-block;">
															<option value="#">Läs/skriv</option>
															<option value="#">Läs</option>
														</select>
													</div>
													
													<div class="col2">
													
													
														<div style="display:block;text-align:right;">
															<button class="button is-tight activate-upcoming">Sätt till kommande</button>
														</div>
														
														
													</div>
												</div>
											
												<div class="spacer16" style="clear:both;"></div>
												
												<div class="expandable-col1">
											
												<?php 
												
													$section_count = 1;
													foreach ($section[$module_count] as $section_item) {
														
												?>
											
												
												
													<h5><?php echo $section_item; ?></h5>
													
													
													<?php
														
														
													
														foreach ($step as $step_row => $step_item) {
															
															
															$step_name = $step_item[0]["name"];
															
															$ordering = explode('.',$step_item[0]["key"]);
															
															$step_link = 'patient-step.php?step='.$step_item[0]["key"];
															
															
															if (($ordering[0] == $module_count) && ($ordering[1] == $section_count)) {
															
															
																$visited = $_SESSION["visited_steps"];
																
																if (strpos($visited, $step_item[0]["key"]) === false) {
																	
																	$is_visited = "";
																	$status_icon = "far fa-circle";
																	
																}
																else {
																	
																	$is_visited = "is-visited";
																	$status_icon = "fas fa-circle";
																}
															
															?>
													
													<div class="step-container <?php echo $is_visited; ?>">
														<div class="step-inside">
														
															<div class="step-guide">
																<h5><a href="<?php echo $step_link; ?>" class="step-link"><?php echo $step_name; ?></a></h5>
															</div>
															
															<div class="step-extra">
																<i class="<?php echo $status_icon; ?>"></i>
															</div>
														
														</div>
													</div><!--step-container-->
													
														<? }  } ?>
													
													
												<?php $section_count++; } ?>
												
												</div><!--expandable-col1-->
												
											
											
												<div class="expandable-col2">
												
													<h5>Att fylla i</h5>
												
												</div><!--expandable-col2-->
											
											
											</div><!--expandable-content-->
										
										</div><!--expandable-container-->
										
										
										
										
										
										
										
										<?php
										
										
										
										$module_count++;
									}
								
								?>
																
								
							
							</div><!--expandable-content-->
						</div><!--expandable-container-->
						
						
						
						
						
						<div class="spacer16"></div>
						
						<div class="expandable-container is-managep">
						
							<div class="expandable-header">
							
								<div class="exh-guide">
									<h3>Mätbatterier</h3>
								</div>
								
								<div class="exh-extra" style="margin-top:5px;">
									
									<div class="exh-info">
										
									</div>
								
									<div class="exh-controls">
										<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
										<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
									</div>
								
								</div>
							
							</div>
							
							<div class="expandable-content">
							
								
								
										
										<div class="expandable-container is-modulep">
										
											<div class="expandable-header">
											
											
												<div class="exh-guide">
													<h3>Pågående mätningar</h3>
												</div>
												
												<div class="exh-extra">
													
													
												
													<div class="exh-controls">
														<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
														<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
													</div>
												
												</div>
											
											</div><!--expandable-header-->
											
											<div class="expandable-content is-primary">
											
											
												<table class="people-table">
												
												<thead>
													<tr>
														<th>Namn</th>
														<th>Startades</th>
														<th>Formulär i detta mätbatteri</th>
														
													</tr>
												</thead>
												<tbody>
													<tr class="tr-odd" style="">
														<td>Veckomätning</td>
														<td>2018-02-04 <a class="button is-small">Stoppa</a></td>
														<td>Hur mår du?, VAS-skala</td>
													</tr>

												</tbody>
												</table>
																							
											
											</div><!--expandable-content-->
										
										</div><!--expandable-container-->
										
										

																
								
							
							</div><!--expandable-content-->
						</div><!--expandable-container-->
						
						
						
						<div class="spacer16"></div>
						
						<div class="expandable-container is-managep">
						
							<div class="expandable-header">
							
								<div class="exh-guide">
									<h3>Länkade moment</h3>
								</div>
								
								<div class="exh-extra" style="margin-top:5px;">
									
									<div class="exh-info">
										
									</div>
								
									<div class="exh-controls">
										<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
										<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
									</div>
								
								</div>
							
							</div>
							
							<div class="expandable-content" style="padding:15px 20px;">
							
								
								
										<form>
										<div class="col1">
											<h3>Länkningar</h3>
											<p>Det finns inga länkade moment.</p>
										</div>
										
										<div class="col2">
											<h3>Lägg till länkning</h3>
											<select>
												<option value="#">Välj moment</option>
												<option value="#">Social fobi</option>
											</select>
											
												<button class="button is-tight" style="display:block;margin-top:8px;">Lägg till</button>
																						
											
										</div>
										</form>
										
										<div class="spacer8" style="clear:both;">
											
										</div>
																
								
							
							</div><!--expandable-content-->
						</div><!--expandable-container-->

						
						<div class="spacer16"></div>
						
						
						<div style="display:block; border: 1px solid #999;padding:16px 24px;background:#fff;max-width:300px; margin:0 auto;text-align:center;">
						
						
							<button class="button is-tight is-alert ajax-popup-link" href="/popup/demo-warning.php">Avsluta</button> <a class="tooltip" title="Informationstext"><i class="fas fa-info-circle"></i></a>&nbsp; &nbsp;<button class="button is-tight is-alert ajax-popup-link" href="/popup/demo-warning.php">Vidmakthåll</button> <a class="tooltip" title="Informationstext"><i class="fas fa-info-circle"></i></a>
						
							
						
						</div>
						
						
					
					
					</div><!--content-level2-->
					
					
										
										
					
				</div><!--main-wide-->
			</div><!--row-main-->
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<?php include '../inc/sob-footer-block.php'; ?> 
		 
		
	</body>
</html>