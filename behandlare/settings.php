<?php

	session_start();
	
	$moment_name = "Behandlare";
	$patient_name = "Ulla Tufvesson";
	$doctor_name = "Therese Bodin";
	$home_link = "/behandlare/";
	$is_doctor = "is-doctor";
	
	$moment[0] = "/social-fobi/";
	
	$menu = array(
		array("item" => "Mina","link" => $home_link),
		array("item" => "Alla","link" => "/behandlare/index-all.php")
	);
	
	
	$menu2 = array(
		array("item" => "Hantera","link" => "/behandlare/patient-manage.php"),
		array("item" => "Meddelanden","link" => "/behandlare/patient-messages.php"),
		array("item" => "Resultat","link" => "/behandlare/patient-results.php"),
	);
	
	
	/* FUNKTIONALITY */
	
	$show_activityplan = true;
	$show_messages = true;
	$show_results = false;
	$show_comments = false;
	$show_comments_forms = false;
	
	/* PRE-FILLED */
	
	$info_caregiver = "
	
	<p>Det behandlingsprogram som du deltar i är uppbyggt efter kognitiv beteendeterapi (KBT) och sträcker sig över åtta veckor. Varje vecka kommer du att få ett nytt kapi­tel att läsa och arbeta med. Kapitlen avslutas med uppgifter som du ska öva på och skicka in till din behandlare. Han eller hon som är din behandlare kommer hjälpa dig att lyckas med övningarna och förklara saker som är svåra att förstå.</p>
	";
	
	
	$step_empty = "
	
	<p>Du använder just nu demo-versionen av Stöd och behandling. Det här steget har ännu inte tilldelats innehåll.</p>
	";
	

	
?>