
<script src="/js/countdown.js"></script>



<div style="position:relative;background:#fff;width:auto;max-width:600px;margin:20px auto;">

	<div style="background:#435b6e;padding:8px 16px;color:#fff"><strong>Videomöte</strong></div>

	<div style="background:#fff;padding:8px 16px;color:#000;"><h3>Ditt videomöte startar om</h3>
	
	
	
		<div id="countdowncontainer" style="font-weight:bold;font-size:1.4em;"></div>

		<script>

		var countdowncontainer = document.getElementById('countdowncontainer') // reference container 

		
		
		var calcdate = new Date(new Date().getTime()+(1.5*24*60*60*1000));
		
		var futuredate = new cdtime(calcdate);
		
		futuredate.oncountdown = function(ms){
			if (ms <= 0){ // if time's up
				countdowncontainer.innerHTML = "Future Date is Here!" // do something
			}
			else{
				var timeleft = cdtime.formatDuration(ms, "days") // format time using formatDuration(timeleftms, "baseunit") function
				countdowncontainer.innerHTML = timeleft.days + ' dagar ' + timeleft.hours + ' timmar ' + timeleft.minutes + ' minuter ' + timeleft.seconds + ' sekunder '
			}
		}
		futuredate.start()

		</script>


		
	
	
		<a class="button" href="/video" target="_blank">Anslut till videomöte<br><span style="font-size:0.7em;">Öppnas i nytt fönster</span></a>
	
	
		<p><a href="/popup/demo-warning.php" class="ajax-popup-link">Information och support</a></p>
		
		<p>Via länken ovan kan du även testa din enhet innan mötet äger rum.</p>
	
	
	
	
	</div>


	
	
	<hr>

	<div style="display:block;text-align:right;padding:8px 16px;">
	
		<a href="javascript:$.magnificPopup.close();">Stäng</a>
	
	</div>

</div>