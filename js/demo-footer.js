
	document.getElementById("wrapper").innerHTML += '<div class="push" id="push"></div>';
	
	function pushFooter() {
		var footerHeight = document.getElementById("footerblock1177").offsetHeight;
		document.getElementById("push").style.height = footerHeight + 'px';
		document.getElementById("wrapper").style.marginBottom = '-' + footerHeight + 'px';
	}
	
	window.addEventListener('resize', pushFooter, false);
	pushFooter();
	
	
		
	
	$( document ).ready(function() {
		
		$('input[type="range"]').rangeslider();
		
		
		$('.activateonload').closest(".expandable-container").find(".control-open").first().toggle();
		$('.activateonload').closest(".expandable-container").find(".control-close").first().toggle();
		$('.activateonload').closest(".expandable-container").find(".expandable-content").first().toggle();
		
		
		$.tooltipster.setDefaults({
			side: 'bottom'
		});
		
		$('.tooltip').tooltipster({
			trigger: 'click'
		});
		
		
		$( ".expandable-header" ).click(function() {
			
			$control_open = $(this).closest(".expandable-container").find(".control-open").first();
			$control_close = $(this).closest(".expandable-container").find(".control-close").first();
			$control_content = $(this).closest(".expandable-container").find(".expandable-content").first();
			
			$control_open.toggle();
			$control_close.toggle();
			$control_content.toggle();
			
			
		});
		
		$( ".step-inside" ).click(function() {
			
			
			$link = $(this).find(".step-link").attr('href');
			window.location = $link;
			
		});
		
		$( ".momenttab-inside" ).click(function() {
			
			
			$link = $(this).find(".momenttab-link").attr('href');
			window.location = $link;
			
			
			
		});
		
		
			
		
		
		$( ".singletab-inside" ).click(function(event) {
			
			
			$gourl = $(this).find(".is-url").attr("href");
			
			if ($gourl) {
				
				window.location = $gourl;
			}
			
			$goclick = $(this).find(".singletab-link");
			
			$goclick.trigger('click');

			return false;
		});
		
		
		
		
		$('textarea.expand').focus(function () {
			$(this).animate({ height: "200px" }, 500);
		});
		
		$('.ajax-popup-link').magnificPopup({
				type: 'ajax'
		});
		
		
		$( ".activate-upcoming" ).click(function() {
			
			
			$("body").overhang({
				activity : "notification",
				message : "Modulen flyttades till kommande",
				easing : "swing",
				col : "#4cd137"
			});
			
		});
		
		$( ".activate-sent" ).click(function() {
			
			
			$("body").overhang({
				activity : "notification",
				message : "Formuläret har skickats (demo)",
				easing : "swing",
				col : "#4cd137"
			});
			
		});
		
		$( ".activate-conversation" ).click(function() {
			
			
			$("body").overhang({
				activity : "notification",
				message : "Konversation skapad! (demo)",
				easing : "swing",
				col : "#4cd137"
			});
			
		});
		
		$( ".activate-completed" ).click(function() {
			
			
			$("body").overhang({
				activity : "notification",
				message : "Modulen flyttades till avklarad",
				easing : "swing",
				col : "#4cd137"
			});
			
		});
		
		
		$(".wrapper").fitVids();
	
	});
	