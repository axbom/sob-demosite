<?php

include ("settings.php");
include ("../inc/Parsedown.php");
include ("../inc/read_contents.php");

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */

?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<?php include ("../inc/1177-header-meta.php"); ?>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			<?php include ("../inc/1177-header-block.php"); ?>
			
			<?php include ("../inc/1177-breadcrumbs.php"); ?>
			
			
			<div class="heading-container">
				<div class="heading">
					<h1><?php echo $moment_name; ?></h1>
				</div>
			</div>
			
			
			<?php 

			include ("../inc/1177-navtabs.php"); ?>
			
			
			<div class="row-main">
				<div class="main-wide square">
										

					<h2 style="margin-top:0;">Meddelanden</h2>
					
					<div style="display:block;text-align:right;"><a href="/popup/new-message.php" class="button ajax-popup-link">Skapa ny konversation</a></div>
					
					<hr>
					
					
					<div class="expandable-container is-messages">
					
						<div class="expandable-header is-messages">
						
							<div class="exh-icon">
								<i class="far fa-comment" data-fa-transform="grow-4"></i>
							</div>
						
							<div class="exh-guide">
								<h3>Välkommen</h3>
							</div>
							
							<div class="exh-extra" style="margin-top:5px;">
								
								<div class="exh-info">
									
								</div>
							
								<div class="exh-controls">
									<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
									<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
								</div>
							
							</div>
							
							<div class="exh-row2">
								<small>Senaste meddelandet 2018-02-28 15:02</small>
							</div>
						
						</div><!--expandable-header-->
						
						<div class="expandable-content is-messages">
						
						
							<div class="sendmessage-container">
							
								<div class="sendmessage-inside">
									
									<form class="form-tight">
									<h5 style="margin:0;">Skriv meddelande</h5>
									
									<textarea style="width:99%;height:20px;" class="expand"></textarea>
									</form>
								
									<div>
									
										<div style="margin:8px 10px;display:inline-block;color:#2e6aa3;"><i class="fas fa-external-link-alt" data-fa-transform="grow-12"></i></div>
										<div style="display:inline-block;float:right;"><a class="button" style="padding:6px 12px;margin:2px;">Skicka</a></div>
										
									</div>
								
								</div>

								
							</div><!--sendmessage-container-->
							
							<hr>
						
							<div class="messagebox is-patient">
							
								<div class="messagebox-header">
								
									<strong><?php echo $patient_name; ?></strong> <small>2018-02-28 15:02</small>
								
								</div>
							
								<div class="messagebox-body">
								
									Tack! Nu börjar jag få koll på det här verktyget. 
								</div>
							
							</div><!--messagebox-->
							
							<div class="messagebox is-doctor">
							
								<div class="messagebox-header">
								
									<strong><?php echo $doctor_name; ?></strong> <small>2018-02-14 09:37</small>
								
								</div>
							
								<div class="messagebox-body">
								
									Fusce eget arcu ac nunc pharetra sagittis. Duis feugiat nisi vel sem fermentum, eu sodales mi interdum. Etiam nec tortor semper neque eleifend luctus. Fusce urna sapien, blandit ut blandit in, tincidunt id magna. Ut iaculis nibh eget maximus dignissim. Proin dignissim ipsum velit, et malesuada velit egestas ut. 
								</div>
							
							</div><!--messagebox-->
						
						</div><!--expandable-content-->
					
					</div><!--expandable-container-->
					
					
					<div class="expandable-container is-messages">
					
						<div class="expandable-header is-messages">
						
							<div class="exh-icon">
								<i class="far fa-comment" data-fa-transform="grow-4"></i>
							</div>
						
							<div class="exh-guide">
								<h3>Vårdsammanfattningar</h3>
							</div>
							
							<div class="exh-extra" style="margin-top:5px;">
								
								<div class="exh-info">
									
								</div>
							
								<div class="exh-controls">
									<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
									<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
								</div>
							
							</div>
							
							<div class="exh-row2">
								<small>Senaste meddelandet 2018-03-14 11:31</small>
							</div>
						
						</div><!--expandable-header-->
						
						<div class="expandable-content is-messages">
						
						
							<div class="sendmessage-container">
							
								<div class="sendmessage-inside">
									
									<form class="form-tight">
									<h5 style="margin:0;">Skriv meddelande</h5>
									
									<textarea style="width:99%;height:20px;" class="expand"></textarea>
									</form>
								
									<div>
									
										<div style="margin:8px 10px;display:inline-block;color:#2e6aa3;"><i class="fas fa-external-link-alt" data-fa-transform="grow-12"></i></div>
										<div style="display:inline-block;float:right;"><a class="button" style="padding:6px 12px;margin:2px;">Skicka</a></div>
										
									</div>
								
								</div>

								
							</div><!--sendmessage-container-->
							
							<hr>
						
							<div class="messagebox is-doctor">
							
								<div class="messagebox-header">
								
									<strong><?php echo $doctor_name; ?></strong> <small>2018-03-14 11:31</small>
								
								</div>
							
								<div class="messagebox-body">
								
									Fusce eget arcu ac nunc pharetra sagittis. Duis feugiat nisi vel sem fermentum, eu sodales mi interdum. Etiam nec tortor semper neque eleifend luctus. Fusce urna sapien, blandit ut blandit in, tincidunt id magna. Ut iaculis nibh eget maximus dignissim. Proin dignissim ipsum velit, et malesuada velit egestas ut. 
								</div>
							
							</div><!--messagebox-->
							
							<div class="messagebox is-doctor">
							
								<div class="messagebox-header">
								
									<strong><?php echo $doctor_name; ?></strong> <small>2018-03-02 09:47</small>
								
								</div>
							
								<div class="messagebox-body">
								
									Fusce eget arcu ac nunc pharetra sagittis. Duis feugiat nisi vel sem fermentum, eu sodales mi interdum. Etiam nec tortor semper neque eleifend luctus. Fusce urna sapien, blandit ut blandit in, tincidunt id magna. Ut iaculis nibh eget maximus dignissim. Proin dignissim ipsum velit, et malesuada velit egestas ut. 
								</div>
							
							</div><!--messagebox-->
							
							<div class="messagebox is-doctor">
							
								<div class="messagebox-header">
								
									<strong><?php echo $doctor_name; ?></strong> <small>2018-02-19 13:23</small>
								
								</div>
							
								<div class="messagebox-body">
								
									Fusce eget arcu ac nunc pharetra sagittis. Duis feugiat nisi vel sem fermentum, eu sodales mi interdum. Etiam nec tortor semper neque eleifend luctus. Fusce urna sapien, blandit ut blandit in, tincidunt id magna. Ut iaculis nibh eget maximus dignissim. Proin dignissim ipsum velit, et malesuada velit egestas ut. 
								</div>
							
							</div><!--messagebox-->
						
						</div><!--expandable-content-->
					
					</div><!--expandable-container-->
					
					
					
				</div><!--main-wide-->
			</div><!--row-main-->
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<?php include '../inc/1177-footer-block.php'; ?>
		
	</body>
</html>