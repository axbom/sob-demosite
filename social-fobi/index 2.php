<?php
session_start();
include ("settings.php");
include ("../inc/Parsedown.php");
include ("../inc/read_contents.php");

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */


?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<?php include ("../inc/1177-header-meta.php"); ?>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			<?php include ("../inc/1177-header-block.php"); ?>
			
			<?php include ("../inc/1177-breadcrumbs.php"); ?>
			
			
			<div class="heading-container">
				<div class="heading">
					<h1><?php echo $moment_name; ?></h1>
				</div>
			</div>
			
			
			<?php include ("../inc/1177-navtabs.php"); ?>
			
			
			<div class="row-main">
				<div class="main-wide square">
				
				
				
					<div class="patientprogress-container" style="margin-bottom:16px; width:100%;position:relative;">
						<div class="patientprogress-inside">
						
							<div class="progressbar-container" style="display:inline-block; vertical-align:middle; border:1px solid #999;width:100%;background:#fff;">
								
								<div class="progressbar-inside" style="border:1px solid #fff;width:80%;background:#128390">
								
									&nbsp;
								</div>
								
								
							</div>
							
							<div>
							 
								12 dagar sedan Social fobi startade (rek. 70 dagar)
							
							</div>
							
							
							
							
						
						</div>
					</div>
					
					
										
					<div class="expandable-container">
					
						<div class="expandable-header">
						
							<div class="exh-icon">
								<i class="fas fa-exclamation" data-fa-transform="grow-4"></i>
							</div>
						
							<div class="exh-guide">
								<h3>Information från vårdgivaren</h3>
							</div>
							
							<div class="exh-extra">
								
								<div class="exh-info">
									<a href="/popup/demo-warning.php" class="ajax-popup-link">Visa snabbguide</a>
								</div>
							
								<div class="exh-controls">
									<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
									<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
								</div>
							
							</div>
						
						</div><!--expandable-header-->
						
						<div class="expandable-content is-primary">
						
							<?php echo $info_caregiver; ?>
							
						</div>
					
					</div><!--expandable-container-->
					
					
					
					
					<div class="expandable-container is-activities">
					
						<div class="expandable-header">
						
							<div class="exh-icon">
								<i class="fas fa-list-ul" data-fa-transform="grow-4"></i>
							</div>
						
							<div class="exh-guide">
								<h3 style="display:inline-block;">Aktiviteter</h3>&nbsp;&nbsp; Nästa aktivitet: 2018-03-31
							</div>
							
							<div class="exh-extra">
								
								<div class="exh-info">
									<a href="/popup/demo-warning.php" class="ajax-popup-link">Historik</a>
								</div>
							
								<div class="exh-controls">
									<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
									<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
								</div>
							
							</div>
						
						</div><!--expandable-header-->
						
						<div class="expandable-content">
						
							<div class="singletab-container">
								<div class="singletab-inside">
									<div class="singletab-label">
										
										<a href="/popup/demo-warning.php" class="singletab-link ajax-popup-link">Utevistelse</a>
										
										
									</div><!-- singletab-label -->
									
									<div class="singletab-extra">
										<div class="singletab-info">
											Nästa: 2018-03-31 09:00
										</div>
										
										<div class="singletab-arrow">
											<i class="fas fa-play" data-fa-transform="grow-2"></i>
										</div>
									</div><!--singletab-extra-->
								</div><!-- singletab-inside -->
							</div><!--singletab-container-->
							
							<div class="singletab-container">
								<div class="singletab-inside">
									<div class="singletab-label">
										
										<a href="/popup/demo-warning.php" class="singletab-link ajax-popup-link">Videomöte</a>
										
										
									</div><!-- singletab-label -->
									
									<div class="singletab-extra">
										<div class="singletab-info">
											Nästa: 2018-04-04 11:30
										</div>
										
										<div class="singletab-arrow">
											<i class="fas fa-play" data-fa-transform="grow-2"></i>
										</div>
									</div><!--singletab-extra-->
								</div><!-- singletab-inside -->
							</div><!--singletab-container-->

							
							
							
						</div><!--expandable-content-->
					
					</div><!--expandable-container-->
					
					
					
					<h2>Aktuellt innehåll</h2>
					
	
					
					<?php 
					
						$module_count=1;
						foreach ($module as $module_item) {
							
							
							
							
							?>
							
							
							
							
							<div class="expandable-container">
							
								<div class="expandable-header">
								
									<div class="exh-icon">
										<i class="fas fa-pencil-alt" data-fa-transform="grow-4"></i>
									</div>
								
									<div class="exh-guide">
										<h3><?php echo $module[$module_count] ?></h3>
									</div>
									
									<div class="exh-extra">
										
										<div class="exh-info">
											7 dagar kvar
										</div>
									
										<div class="exh-controls">
											<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
											<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
										</div>
									
									</div>
								
								</div><!--expandable-header-->
								
								<div class="expandable-content is-primary">
								
								
									<div class="expandable-col1">
								
									<?php 
									
										$section_count = 1;
										foreach ($section[$module_count] as $section_item) {
											
									?>
								
									
									
										<h5><?php echo $section_item; ?></h5>
										
										
										<?php
											
											
										
											foreach ($step as $step_row => $step_item) {
												
												
												$step_name = $step_item[0]["name"];
												
												$ordering = explode('.',$step_item[0]["key"]);
												
												$step_link = 'step.php?step='.$step_item[0]["key"];
												
												
												if (($ordering[0] == $module_count) && ($ordering[1] == $section_count)) {
												
												
													$visited = $_SESSION["visited_steps"];
													
													if (strpos($visited, $step_item[0]["key"]) === false) {
														
														$is_visited = "";
														$status_icon = "far fa-circle";
														
													}
													else {
														
														$is_visited = "is-visited";
														$status_icon = "fas fa-circle";
													}
												
												?>
										
										<div class="step-container <?php echo $is_visited; ?>">
											<div class="step-inside">
											
												<div class="step-guide">
													<h5><a href="<?php echo $step_link; ?>" class="step-link"><?php echo $step_name; ?></a></h5>
												</div>
												
												<div class="step-extra">
													<i class="<?php echo $status_icon; ?>"></i>
												</div>
											
											</div>
										</div><!--step-container-->
										
											<? }  } ?>
										
										
									<?php $section_count++; } ?>
									
									</div><!--expandable-col1-->
									
								
								
									<div class="expandable-col2">
									
										<h5>Att fylla i</h5>
										
										
										<div class="step-container">
											<div class="step-inside">
											
												<div class="step-guide">
													<h5><a href="step.php?step=2.2.1" class="step-link">Min sociala ångest</a></h5>
												</div>
												
												<div class="step-extra">
													<i class="far fa-circle"></i>
												</div>
											
											</div>
										</div><!--step-container-->
										
										<div class="step-container">
											<div class="step-inside">
											
												<div class="step-guide">
													<h5><a href="step.php?step=2.2.2" class="step-link">Sortera dina tankar</a></h5>
												</div>
												
												<div class="step-extra">
													<i class="far fa-circle"></i>
												</div>
											
											</div>
										</div><!--step-container-->
										
										
									
									</div><!--expandable-col2-->
								
								
								</div><!--expandable-content-->
							
							</div><!--expandable-container-->
							
							
							
							
							
							
							
							<?php
							
							
							
							$module_count++;
						}
					
					?>
					
										
					
				</div><!--main-wide-->
			</div><!--row-main-->
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<?php include '../inc/1177-footer-block.php'; ?>
		
		
	</body>
</html>