<?php

include ("settings.php");
include ("../inc/Parsedown.php");
include ("../inc/read_contents.php");

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */

?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<?php include ("../inc/1177-header-meta.php"); ?>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			<?php include ("../inc/1177-header-block.php"); ?>
			
			<?php include ("../inc/1177-breadcrumbs.php"); ?>
			
			
			<div class="heading-container">
				<div class="heading">
					<h1><?php echo $moment_name; ?></h1>
				</div>
			</div>
			
			
			<?php 

			include ("../inc/1177-navtabs.php"); ?>
			
			
			<div class="row-main">
				<div class="main-wide square">
										

					<h2 style="margin-top:0;">Resultat</h2>
					
					<p>Här visas de värden från dina svar som behandlaren valt att visa upp som diagram och tabeller. Du kan se dina svar över tid.</p>

					
					<div class="expandable-container">
					
						<div class="expandable-header">
						
							
						
							<div class="exh-guide">
								<h3>Sömnloggen</h3>
							</div>
							
							<div class="exh-extra" style="margin-top:5px;">
								
								<div class="exh-info">
									
								</div>
							
								<div class="exh-controls">
									<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
									<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
								</div>
							
							</div>
							
							
						
						</div><!--expandable-header-->
						
						<div class="expandable-content is-primary">
						
						
							
							<h3>Antal timmar sömn</h3>
							
							<div class="col1">
							
								<img src="/img/diagram-sleep.png" alt="diagram" style="max-width:100%">
							
							</div>
							
							<div class="col2">
							
								<table class="data-table">
									<thead>
										<tr>
											<th>Datum/tid</th><th>Värde</th>
										</tr>
									</thead>
									<tbody>
										
											<tr><td>2018-04-18</td><td>5</td></tr>
											<tr><td>2018-04-19</td><td>4</td></tr>
											<tr><td>2018-04-20</td><td>7</td></tr>
											<tr><td>2018-04-21</td><td>3</td></tr>
											<tr><td>2018-04-22</td><td>8</td></tr>
											<tr><td>2018-04-23</td><td>6</td></tr>
										
									</tbody>
								</table>
							
							</div>
							
						
						</div><!--expandable-content-->
					
					</div><!--expandable-container-->
					
					
					
					
					
					
				</div><!--main-wide-->
			</div><!--row-main-->
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<?php include '../inc/1177-footer-block.php'; ?>
		
	</body>
</html>