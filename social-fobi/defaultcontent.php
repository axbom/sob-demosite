<?php
	$step_empty = '
		
		<div class="columns">
		
			<div class="col1">
				<p>Du använder just nu demo-versionen av Stöd och behandling. Här visas exempel på typer av innehåll som en sida kan innehålla.</p>
		
				<p>En sida kan bestå av text, bilder, filmer, formulär (mätningar, instrument och frågeformulär) och valfri kombination av alla dessa element. En så kallad designer bygger upp sidan baserat på dessa tillgängliga komponenter för att på bästa sätt förmedla kunskap, lärande och inhämta information vid behov. Bilder kan placeras så att texten flödar vid sidan av bilden, samtidigt som text och bild hamnar ovanför varandra på små skärmar.<p>
		
			</div>
		
			<div class="col2">
				<img src="https://place.cat/bw/500/300" style="max-width:100%">
		
			</div>
		
		</div>
		
		<p>Ett typiskt formuär kan innehålla fält för textinmatning, flervalsfrågor och fler möjligheter som illustreras av detta exempel:</p>
		
		<hr>
		
		<h2>Dagbok</h2>
		
		<form>
		
			<label for="f1">Beskriv din dag</label>
			<textarea id="f1"></textarea>
			<div class="spacer16"></div>
			<label for="f2">Hur mår du? (Skala 1-10 där 1 är mycket dåligt och 10 är mycket bra)</label><br>
			<input type="range" min="1" max="10" step="1" value="5" id="f2" style="display:inline-block;">
			<div class="spacer16"></div>
			<label for="f3">Hur många timmar sov du i natt?</label>
			<input type="text" id="f3">
			
			<div><a class="button activate-sent">Skicka</a></div>
		
		</form>
		
		<hr>
		
		
		
		
		
		
		
			<h3>Att vara blyg del 1</h3>
			
			<p>Exempel på hur en film kan presenteras som en del av innehållet.</p>

			 <iframe width="560" height="315" src="https://www.youtube.com/embed/4bw6TXGaOWY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

		
		';

?>