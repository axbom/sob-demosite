<?php

include ("settings.php");
include ("../inc/Parsedown.php");
include ("../inc/read_contents.php");

/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */


?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<?php include ("../inc/1177-header-meta.php"); ?>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			<?php include ("../inc/1177-header-block.php"); ?>
			
			<div class="breadcrumbs-container">
				<div class="breadcrumbs">
					<a href="/">DEMO</a> » Stöd och behandling
				</div><!--breadcrumbs-->
			</div><!--breadcrumbs-container-->
			
			
			<div class="heading-container">
				<div class="heading">
					<h1>Stöd och behandling</h1>
				</div>
			</div>
			
			
			<div class="columns">
			
				<div class="col1">
				
					<div class="card" style="background:#fff;">
					
						<h2>Dina pågående stöd eller behandlingsprogram</h2>
						
						<div class="spacer16"></div>
						
						
						
						<div class="singletab-container is-moment">
							<div class="singletab-inside">
								<div class="singletab-label">
									
									<a href="/social-fobi/" class="singletab-link is-url">Social fobi</a>
									
									
								</div><!-- singletab-label -->
								
								<div class="singletab-extra">
									
									
									<div class="singletab-arrow">
										<i class="fas fa-arrow-circle-right" data-fa-transform="grow-8"></i>
									</div>
								</div><!--singletab-extra-->
							</div><!-- singletab-inside -->
						</div><!--singletab-container-->
						
						<div class="singletab-container is-moment">
							<div class="singletab-inside">
								<div class="singletab-label">
									
									<a href="/halsoplan/" class="singletab-link is-url">Min hälsoplan</a>
									
									
								</div><!-- singletab-label -->
								
								<div class="singletab-extra">
									
									
									<div class="singletab-arrow">									
										<i class="fas fa-arrow-circle-right" data-fa-transform="grow-8"></i>
									</div>
								</div><!--singletab-extra-->
							</div><!-- singletab-inside -->
						</div><!--singletab-container-->
					
					</div>
					
					
					<div class="card" style="background:#fff;">
					
						<h2>Tillgängliga stöd eller behandlingsprogram</h2>
						
						<div class="spacer16"></div>
						
						
						
						<div class="expandable-container is-caregiver">
						
							<div class="expandable-header is-caregiver">
							
								
							
								<div class="exh-guide">
									<h3>Landstinget Sörmland</h3>
								</div>
								
								<div class="exh-extra">
									
									
								
									<div class="exh-controls">
										<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
										<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
									</div>
								
								</div>
							
							</div><!--expandable-header-->
							
							<div class="expandable-content is-primary">
							
								<p>Här kan du se utbildningar, anmälan eller screeningar som du själv har möjlighet att starta.</p>
								<div class="spacer16"></div>
								
								<div class="expandable-container is-caregiver2">
								
									<div class="expandable-header is-caregiver2">
									
										
									
										<div class="exh-guide">
											<h3>Depressionsbehandling</h3>
										</div>
										
										<div class="exh-extra">
											
											
										
											<div class="exh-controls">
												<i class="fas fa-angle-down control-open" data-fa-transform="grow-24"></i>
												<i class="fas fa-angle-up control-close" data-fa-transform="grow-24"></i>
											</div>
										
										</div>
									
									</div><!--expandable-header-->
									
									<div class="expandable-content is-caregiver2">
									
										<p>Ibland ska du kunna komma på ett fysiskt besök till mottagningen. Detta framgår i så fall av presentationen. Välj då en mottagning som du har möjlighet att besöka.</p>
										
										
										<div class="spacer16"></div>
										
										
										
										<div class="singletab-container is-moment">
											<div class="singletab-inside">
												<div class="singletab-label">
													
													<a href="/popup/demo-warning.php" class="singletab-link ajax-popup-link">Katthults vårdcentral</a>
													
													
												</div><!-- singletab-label -->
												
												<div class="singletab-extra">
													
													
													<div class="singletab-arrow">
														<i class="fas fa-arrow-circle-right" data-fa-transform="grow-8"></i>
													</div>
												</div><!--singletab-extra-->
											</div><!-- singletab-inside -->
										</div><!--singletab-container-->
										
										<div class="singletab-container is-moment">
											<div class="singletab-inside">
												<div class="singletab-label">
													
													<a href="/popup/demo-warning.php" class="singletab-link ajax-popup-link">Vimmerby internetmottagning</a>
													
													
												</div><!-- singletab-label -->
												
												<div class="singletab-extra">
													
													
													<div class="singletab-arrow">									
														<i class="fas fa-arrow-circle-right" data-fa-transform="grow-8"></i>
													</div>
												</div><!--singletab-extra-->
											</div><!-- singletab-inside -->
										</div><!--singletab-container-->

										
										
										
									</div>
								
								</div><!--expandable-container-->
								
								
								
								
							</div>
						
						</div><!--expandable-container-->
						
					
					</div>
				
				
				</div><!--col1-->
				
				
				<div class="col2">
					
					<div class="card" style="background:#fff;">
					
						<h2>Om Stöd och behandling</h2>
						
						<p>Välkommen till Stöd och behandling, en tjänst där du kan få stöd, behandling eller utbildning på nätet. <a class="footer-link-text ajax-popup-link" href="/popup/demo-warning.php">Läs mer om Stöd och behandling</a>.</p>

						<p><a class="footer-link-text ajax-popup-link" href="/popup/demo-warning.php">Mer om regler och rättigheter</a></p>
						
						
						<iframe width="480" height="270" src="https://www.youtube.com/embed/J3vaMgktUP4?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>	
					
					</div>
					
				</div><!--col2-->
			
			</div><!--columns-->
			
			
			
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<?php include '../inc/1177-footer-block.php'; ?>
		
	</body>
</html>