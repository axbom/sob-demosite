<?php



/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */


?>

<!DOCTYPE html>
<html class="no-js" lang="sv-SE">

<head>
		
	
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<title><?php echo $moment_name; ?> | DEMO: 1177 Vårdguiden e-tjänster</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
														
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />

	<?php

	$date=new DateTime(); //this returns the current date time
	$datenow = $date->format('Y-m-d-H-i-s');
	$active_home = false;

	?>

	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css">
	<link rel="stylesheet" type="text/css" href="css/rangeslider.css">
	<link rel="stylesheet" type="text/css" href="css/tooltipster.bundle.min.css">
	<link rel="stylesheet" type="text/css" href="css/overhang.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css?<?php echo $datenow; ?>">


	<script src="js/fontawesome-all.min.js" charset="utf-8"></script>

	<script src="js/jquery-3.3.1.min.js"></script>

</head>
	<body>
		
		<div class="wrapper" id="wrapper">
		
			
			
			<header class="_1177-header-container" id="1177-topbar">
				<div class="_1177-header">
					<div class="_1177-header-content">
					<div class="_1177-logo-block">
						<h1 class="_1177-sitetitle"><img class="_1177-logo-img" alt="1177 Vårdguiden" src="../img/1177-logo.png"><span class="_1177-sitename">DEMO: Stöd och behandling</span></h1>
					</div>
					
					<div class="_1177-user">
						<div class="_1177-settings">
							<a class="link-userblock ajax-popup-link" href="popup/demo-warning.php">Inställningar</a>
						</div>
						<div class="_1177-logout">
							<i class="fas fa-sign-out-alt" style="color:#fff;"></i> <a class="link-userblock ajax-popup-link" href="popup/demo-warning.php">Logga ut</a>
						</div>
						<div class="_1177-currentuser"><?php if (isset($patient_name)) { ?>Inloggad som&nbsp;<?php echo $patient_name;  } ?></div>
					</div><!--_1177-user-->
					</div><!--_1177-header-content-->
				</div><!--_1177-header-->
			</header>
			
			
			
			<div class="breadcrumbs-container">
				<div class="breadcrumbs">
					DEMO
				</div><!--breadcrumbs-->
			</div><!--breadcrumbs-container-->
			
			
			<div class="heading-container">
				<div class="heading">
					<h1><?php echo $moment_name; ?></h1>
				</div>
			</div>

			
			

			
			
			<div class="row-main">
				<div class="main-medium square">
				
					<h2>DEMO: Stöd och behandling</h2>
					
					<div class="margin:0 auto;">
						<img src="img/demo-socialfobi.png" alt="" style="max-width:500px;">
					</div>
	
					<p><strong>Välkommen till DEMO-versionen av Stöd och behandling.</strong> Här kan du själv klicka runt och få en uppfattning om hur systemet fungerar. Till att börja med kan du prova att använda systemet både som patient och som vårdpersonal. Vi kommer löpande fylla på med exempel-innehåll.</p>
					
					
					<p style="background:#EF9797; padding:10px; margin:16px 0;"><strong>Observera att inget du gör här sparas i en databas och inget skickas till någon vårdgivare. Viss data sparas temporärt i din webbläsare (med hjälp av cookies) för att simulera interaktiva funktioner i DEMO:n, men denna data/information försvinner så fort du stänger ner din webbläsare.</strong></p>
					
					<hr>
					
					<h2>Välj vilken del du vill testa</h2>
					
					<p><strong>Tips!</strong> Du kan öppna flera vyer i olika flikar i din webbläsare så att du kan byta och se hur ändringar i det ena gränssnittet påverkar det andra</p>
					
					
					
					<div class="momenttab-container">
					
						<div class="momenttab-inside" >
							
							<div class="momenttab-icon">
							
								<span class="fa-layers fa-fw">
									
									
									<i class="fas fa-play" data-fa-transform="shrink-8 right-1"></i>
									
									<i class="fas fa-redo-alt" data-fa-transform="grow-14 rotate--135"></i>	
									
									
								</span>
							
							
								
							
							</div>
							
							<div class="momenttab-name">
							
								<a href="invanare/" class="momenttab-link">Invånare</a>
								
							</div>
							
							<div class="momenttab-arrow">
							
								<i class="fas fa-angle-right" data-fa-transform="grow-36" style="color: #5d7c8b;"></i>
							
							</div>
							
						</div>
					
					</div><!-- momenttab-container -->
					
					
					<div class="momenttab-container">
					
						<div class="momenttab-inside">
							
							<div class="momenttab-icon">
							
								<span class="fa-layers fa-fw">
									
									
									<i class="fas fa-play" data-fa-transform="shrink-8 right-1"></i>
									
									<i class="fas fa-redo-alt" data-fa-transform="grow-14 rotate--135"></i>	
									
									
								</span>
							
							
								
							
							</div>
							
							<div class="momenttab-name">
							
								<a href="behandlare/" class="momenttab-link">Behandlare</a>
								
							</div>
							
							<div class="momenttab-arrow">
							
								<i class="fas fa-angle-right" data-fa-transform="grow-36" style="color: #5d7c8b;"></i>
							
							</div>
							
						</div>
					
					</div><!-- momenttab-container -->
					
					
					
					<hr>
					
					<div style="margin:24px 0;">
					
					<h3>Rensa minnet / radera data</h3>
					
					<p>För att bidra till förståelsen är denna DEMO i viss mån interaktiv och <strong>sparar innehåll temporärt i din egen webbläsare</strong> (exempelvis markeras sidor som du besökt och "moduler" kan aktiveras och deaktiveras). Om du aktivt vill rensa bort (nollställa) innehållet till ursprungsläget utan att starta om webbläsaren kan du klicka här:<br><br><a href="popup/session-destroy.php" class="button is-tight ajax-popup-link">Rensa minnet</a></p>
					
					</div>
					
					<hr>
					
					
					<h2>Lär dig mer</h2>
					
					<p>Här hittar du mer information om vad Stöd och behandling är och kan användas till.</p>
					
					<ul>
						<li><a href="https://skl.se/halsasjukvard/ehalsa/internetbaseratstodochbehandling.internetbaseratstodochbehandling.html">Internetbaserat Stöd och behandling (SKL)</a></li>
						<li><a href="https://www.inera.se/tjanster/stod_behandling/">Införande och teknisk lösning (Inera)</a></li>
					</ul>
					
					
					<h3>Filmer</h3>
					
					<p>Filmer som beskriver mer om Stöd och behandling, både ur ett användarperspektiv och ett tekniskt perspektiv.</p>
					<div class="spacer16"></div>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/X2tHTsdBTQc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					
					<div class="spacer16"></div>
					
					<iframe width="480" height="270" src="https://www.youtube.com/embed/jLky0M8wIQI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>	
					<div class="spacer16"></div>
					<iframe width="480" height="270" src="https://www.youtube.com/embed/J3vaMgktUP4?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>	
					<div class="spacer16"></div>
					<iframe src="https://player.vimeo.com/video/139173041" width="480" height="270" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									
				</div><!--medium-wide-->
			</div><!--row-main-->
		</div><!--wrapper-->
		
		<!-- FOOTER STARTS HERE -->
		
		<div class="footerblock1177" id="footerblock1177">
			<div class="_1177-footer">
				
				<div class="_1177-links">
					<div class="_1177-links-row">
						<div class="footer-link" id="link-phone">
							<img class="icon-footer" src="../img/icon-phone.png" alt=""><a class="footer-link-text ajax-popup-link" href="../popup/demo-warning.php">Ring supporten 08-123 XXX XX</a>
						</div>
						<div class="footer-link" id="link-support">
							<img class="icon-footer" src="../img/icon-mail.png" alt=""><a class="footer-link-text link1177 ajax-popup-link" href="../popup/demo-warning.php" target="_blank" title="Öppnas i nytt fönster">Mejla supporten</a><img src='../img/icon-newwindow.png' class='extlink' alt='Länken öppnas i nytt fönster.'>
						</div>
						<div class="footer-link" id="link-1177more">
							<img class="icon-footer" src="../img/icon-q.png" alt=""><a class="footer-link-text link1177" href="http://1177.se/e-tjanster" target="_blank" title="Öppnas i nytt fönster">Läs mer om e-tjänster</a><img src='../img/icon-newwindow.png' class='extlink' alt='Länken öppnas i nytt fönster.'>
						</div>
						<div class="footer-link" id="link-sitemap">
							<img class="icon-footer" src="../img/icon-list.png" alt=""><a class="footer-link-text ajax-popup-link" href="../popup/demo-warning.php">Webbkarta</a>
						</div>
					</div>
				</div>
				
				<div class="_1177-tagline-block">
					<div class="_1177-tagline-text"><a class="link-negative link1177i" title="Öppnas i nytt fönster" href="http://1177.se" target="_blank">1177 Vårdguiden</a>&nbsp;<img src='../img/icon-newwindow-inv.png' class='extlink' alt='Länken öppnas i nytt fönster.'> —&nbsp;En&nbsp;tjänst&nbsp;från&nbsp;Sveriges&nbsp;landsting&nbsp;och&nbsp;regioner</div>
				</div>
			</div>
		</div>

		<script src="js/jquery.magnific-popup.js"></script>
		<script src="js/tooltipster.bundle.min.js"></script>
		<script src="js/rangeslider.min.js"></script>
		<script src="js/overhang.min.js"></script>
		<script src="js/jquery.fitvids.js"></script>
		<script src="js/demo-footer.js?<?php echo $datenow; ?>"></script>
		
	</body>
</html>