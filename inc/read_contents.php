<?php

		if ($moment_name == "Behandlare") {
			
			$contents_file = '..' . $moment[0] .'contents.txt';
			$defaultcontent = '..' . $moment[0] .'defaultcontent.php';
		}
		
		else {
			$contents_file = 'contents.txt';
			$defaultcontent = 'defaultcontent.php';
		}
		
		$txt_file    = file_get_contents($contents_file);
		include($defaultcontent);
		
		
		
		$rows        = explode("\n", $txt_file);
		
		$no_module = 0;
		$no_section = 0;
		$no_step = 0;
		$steph = 0;
		$contentsbuilder = '';


		foreach($rows as $row => $data)
		{
			//get row data
			
			$htest = substr($data,0,3);
			

			
			if ($htest == '###') {
				
				
				$no_step++;
				$steph++;
				$stepcontents[$no_step]['contents'] = $contentsbuilder;
				$contentsbuilder = '';
				
				$stepkey = $no_module . '.' . $no_section . '.' . $no_step;
				
				
				$step[$steph] = [
					
					array ('key' => $stepkey, 'module' => $module_item, 'section' => $section_item, 'name' => substr($data,3))
					
				];

				
			}
			elseif (substr($htest,0,2) == '##') {
				
				
				$no_section++;
				
				$stepcontents[$no_step]['contents'] = $contentsbuilder;
				$contentsbuilder = '';
				
				$section_item = substr($data,2);
					
				$section[$no_module][$no_section] = substr($data,2);
				
				$no_step = 0;

			}
			elseif (substr($htest,0,1) == '#') {
				
				$no_module++;
				
				$stepcontents[$no_step]['contents'] = $contentsbuilder;
				$contentsbuilder = '';
				
				$module_item = substr($data,1);
						
				$module[$no_module] = substr($data,1);

				$no_step = 0;
				$no_section = 0;
			}
			else {
				$contentsbuilder .= $data;
			}

		}
	
?>