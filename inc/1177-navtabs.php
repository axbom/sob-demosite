<div class="navtabs-container">
	<div class="navtabs">
	
		<ul class="navtabs-list">
		
			<?php foreach ($menu as $key => $value) {
											
				$is_active = '';
				
				if ($value["link"] == $_SERVER['REQUEST_URI'] ) {
					
					$is_active = "is-active";
					
				}
				elseif (($active_home) && ($value["link"] == $home_link)) {
					
					$is_active = "is-active";
				}
				
				
				$messages = strpos($value["link"],'messages');
				
				if ($messages !== false) {
					
					$is_messages = "is-messages";
					$mess_counter = '<div style="position:absolute; right:4px;top:-6px;">
					
					
						<span class="fa-layers fa-fw">
							<i class="fas fa-circle" style="color:#e65557;" data-fa-transform="grow-5"></i>
							<span class="fa-layers-text fa-inverse" data-fa-transform="shrink-3 right-1" style="font-weight:900">1</span>
						</span>
					
					
					</div>';

				}
				else {
				
					$is_messages = "";
					$mess_counter = "";
				
				}
				
			?>
		
				<li class="navtabs-item <?php echo $is_active; ?> <?php echo $is_doctor; ?> <?php echo $is_messages; ?>"><a href="<?php echo $value["link"]; ?>"><?php echo $value["item"]; ?></a><?php echo $mess_counter; ?></li>
			
			<?php } ?>
			
		</ul>
	
	</div>
</div>