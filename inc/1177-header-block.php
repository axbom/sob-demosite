<header class="_1177-header-container" id="1177-topbar">
	<div class="_1177-header">
		<div class="_1177-header-content">
		<div class="_1177-logo-block">
			<h1 class="_1177-sitetitle"><img class="_1177-logo-img" alt="1177 Vårdguiden" src="../img/1177-logo.png"><span class="_1177-sitename">DEMO: Stöd och behandling</span></h1>
		</div>
		
		<div class="_1177-user">
			<div class="_1177-settings">
				<a class="link-userblock ajax-popup-link" href="../popup/demo-warning.php">Inställningar</a>
			</div>
			<div class="_1177-logout">
				<i class="fas fa-sign-out-alt" style="color:#fff;"></i> <a class="link-userblock ajax-popup-link" href="../popup/demo-warning.php">Logga ut</a>
			</div>
			<div class="_1177-currentuser"><?php if (isset($patient_name)) { ?>Inloggad som&nbsp;<?php echo $patient_name;  } ?></div>
		</div><!--_1177-user-->
		</div><!--_1177-header-content-->
	</div><!--_1177-header-->
</header>