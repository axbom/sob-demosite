<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title><?php echo $moment_name; ?> | DEMO: 1177 Vårdguiden e-tjänster</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
													
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />

<?php

$date=new DateTime(); //this returns the current date time
$datenow = $date->format('Y-m-d-H-i-s');
$active_home = false;

?>

<link rel="stylesheet" type="text/css" href="../css/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="../css/rangeslider.css">
<link rel="stylesheet" type="text/css" href="../css/tooltipster.bundle.min.css">
<link rel="stylesheet" type="text/css" href="../css/overhang.min.css">
<link rel="stylesheet" type="text/css" href="../css/style.css?<?php echo $datenow; ?>">


<script src="../js/fontawesome-all.min.js" charset="utf-8"></script>

<script src="../js/jquery-3.3.1.min.js"></script>